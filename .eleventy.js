const CleanCSS = require('clean-css');
const UglifyJS = require('uglify-js');
const htmlMinifier = require('html-minifier');

module.exports = function(eleventyConfig) {
	eleventyConfig.addFilter('cssmin', function(code) {
		return new CleanCSS({}).minify(code).styles;
	});

	eleventyConfig.addFilter('jsmin', function(code) {
		return UglifyJS.minify(code).code;
	});

	eleventyConfig.addTransform('html-minify', async function(content, outputPath) {
		if(outputPath && outputPath.endsWith('.html')) {
			let minified = htmlMinifier.minify(content, {
				useShortDoctype: true,
				removeComments: true,
				collapseWhitespace: true
			});
			return minified;
		}
		return content;
	});

	eleventyConfig.addPassthroughCopy('content/**/*.ico');
	eleventyConfig.addPassthroughCopy('content/**/*.jpg');
	eleventyConfig.addPassthroughCopy('content/**/*.png');
	eleventyConfig.addPassthroughCopy('content/**/*.svg');

	return {
		dir: {
			input: 'content',
			output: 'dist'
		}
	}
};