---
title: Privacy info
---
Definition of "Sites": This site (nast.ro) and any of its subdomains (such as
urlencode.nast.ro)

The Sites (*.nast.ro) only use technical cookies from
[Cloudflare](https://www.cloudflare.com/privacypolicy/) (if any).

The Sites may use other technologies similar to cookies (localStorage and
sessionStorage) to store application state and preferences.

The Sites use [Plasuible Analytics](https://plausible.io/data-policy) to receive
information on how many people visit the Sites, which pages they visit, and
where they come from.  
Plausible does not collect personal data and does not use cookies or other
similar technologies.  
The Sites save the user's choice on enabling or disabling Plausible using
localStorage (similar to cookies).

The Sites are hosted on [Netlify](https://www.netlify.com/gdpr-ccpa) and use
[Cloudflare](https://www.cloudflare.com/privacypolicy/).

To contact the site owner please see <https://qlcvea.it/contact>.

## Site-specific information

The following information only applies to specific sites.

### base64.nast.ro

This site uses localStorage to save preferences and to save the page content
(inputted text) between sessions.

The information stored by this last feature can be deleted from your device by
disabling "Save content" within the [webapp's
settings](https://base64.nast.ro/#setup).

After updating analytics preferences, the site will use sessionStorage to store
the current application state (inputted text) and restore it after reloading the
page.

### makeqr.nast.ro

This site uses localStorage to save preferences and to save the page content
(inputted text) between sessions.

The information stored by this last feature can be deleted from your device by
disabling "Save content" within the [webapp's
settings](https://makeqr.nast.ro/#setup).

After updating analytics preferences, the site will use sessionStorage to store
the current application state (inputted text) and restore it after reloading the
page.

### scanqr.nast.ro

This site uses localStorage to save preferences.

After updating analytics preferences, the site will use sessionStorage to store
the current application state (inputted text) and restore it after reloading the
page.

This site requests permission to access the camera to scan QR codes. The camera
feed is only processed on the device the page has been loaded on and is never
sent outside of it.

### urlencode.nast.ro

This site uses localStorage to save preferences and to save the page content
(inputted text) between sessions.

The information stored by this last feature can be deleted from your device by
disabling the feature using the "Save content" toggle within the [webapp's
settings](https://urlencode.nast.ro/#setup).

After updating analytics preferences, the site will use sessionStorage to store
the current application state (inputted text) and restore it after reloading the
page.

[Homepage](/) - [Change analytics preferences](#privacysetup)