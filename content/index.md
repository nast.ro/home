# Hello!

- [Base64 encoder/decoder](https://base64.nast.ro)
- [QR code maker](https://makeqr.nast.ro)
- [QR code scanner](https://scanqr.nast.ro)
- [URL encoder/decoder](https://urlencode.nast.ro)

[Privacy info](/privacy/) - [Change analytics preferences](#privacysetup)