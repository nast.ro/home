// Add other functionality in separate sets of (function() {})();

// Privacy information
(function() {
	let allowAnalyticsCheckbox, privacySetupContent, privacySaveButton;

	let changed = false;
	let readHash = () => {
		let hash = window.location.hash;
    if (hash.startsWith('#')) hash = hash.substring(1);
    if (hash.toLowerCase() === 'privacysetup') {
			privacySetupContent.style.display = '';
      changed = false;
    } else {
			privacySetupContent.style.display = 'none';
      changed = false;
    }
	};
	window.addEventListener('hashchange', readHash);
	window.addEventListener('load', () => {
		allowAnalyticsCheckbox = document.getElementById('pc');
		privacySetupContent = document.getElementById('p');
		privacySaveButton = document.getElementById('p').getElementsByTagName('button')[0];

		allowAnalyticsCheckbox.addEventListener('change', function() {
			window.localStorage.setItem('allow-analytics', this.checked ? 'true' : 'false');
			changed = true
		});
		privacySaveButton.addEventListener('click', () => {
			if (changed) {
				allowAnalyticsCheckbox.disabled = true;
				privacySaveButton.disabled = true;
				let url = new URL(window.location.toString());
				url.hash = '';
				window.location.href = url.href;
			} else {
				window.history.back();
			}
		});

		readHash();

		if (window.localStorage.getItem('allow-analytics') === null) {
      // Not configured yet, show the panel and write default settings
      changed = true;
			privacySetupContent.style.display = '';
      window.localStorage.setItem('allow-analytics', 'true');
    } else if (window.localStorage.getItem('allow-analytics') === 'true') {
			// Load the analytics script
			let analyticsScript = document.createElement('script');
			analyticsScript.src = 'https://plausible.io/js/plausible.js';
			analyticsScript.dataset.domain = window.location.hostname;
			document.head.appendChild(analyticsScript);
		}

		allowAnalyticsCheckbox.checked = window.localStorage.getItem('allow-analytics') === 'true';
	});
})();
